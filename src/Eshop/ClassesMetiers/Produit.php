<?php

namespace Eshop\ClasseMetiers;

use Eshop\Db\DBA;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Produit
 *
 * @author sio
 */
class Produit {

    private $idProduit;
    private $libelle;
    private $description;
    private $prix;
    private $image;
    private $categorie;
    private $marque;

    public function getMarque() {
        return $this->marque;
    }

    public function setMarque($marque = NULL) {
        $this->marque = $marque;
        if ($marque != null) {
            $marque->addProduit($this);
        }
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function setCategorie($categorie = NULL) {
        $this->categorie = $categorie;
        if ($categorie != null) {
            $categorie->addProduit($this);
        }
    }

    public function compareTo(Produit $produit) {
        return $produit->idProduit == $this->idProduit;
    }

    public function getIdProduit() {
        return $this->idProduit;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function getImage() {
        return $this->image;
    }

    public function setIdProduit($idProduit) {
        $this->idProduit = $idProduit;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setPrix($prix) {
        $this->prix = $prix;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public static function fetchAllByCategorie(Categorie $categorie) {
        $idCategorie = $categorie->getIdCategorie();
        $collectionProduit = array();
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectByIdCategorie);
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduitCategorie($record, $categorie);
        }
        return $collectionProduit;
    }

    public static function fetchAllByMarque(Marque $marque) {
        $idMarque = $marque->getIdMarque();
        $collectionProduit = array();
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectByIdMarque);
        $pdoStatement->bindParam(":idMarque", $idMarque);
        $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduitMarque($record, $marque);
        }
        return $collectionProduit;
    }

    private static function arrayToProduit(Array $array, Categorie $categorie = NULL, Marque $marque = NULL) {
        $p = new Produit();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        if ($categorie == NULL) {
            if ($array["idCategorie"] != NULL) {
                $p->categorie = Categorie::fetch($array["idCategorie"]);
            } else {
                $p->categorie = null;
            }
        } else {
            $p->categorie = $categorie;
        }
        if ($marque == NULL) {
            if ($array["idMarque"] != NULL) {
                $p->marque = Marque::fetch($array["idMarque"]);
            } else {
                $p->marque = null;
            }
        } else {
            $p->marque = $marque;
        }
        return $p;
    }

    private static function arrayToProduitMarque(Array $array, Marque $marque = NULL) {
        $p = new Produit();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        if ($marque == NULL) {
            if ($array["idMarque"] != NULL) {
                $p->marque = Marque::fetch($array["idMarque"]);
            } else {
                $p->marque = null;
            }
        } else {
            $p->marque = $marque;
        }
        return $p;
    }

    private static function arrayToProduitCategorie(Array $array, Categorie $categorie) {
        $p = new Produit();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        if ($categorie == NULL) {
            if ($array["idCategorie"] != NULL) {
                $p->categorie = Categorie::fetch($array["idCategorie"]);
            } else {
                $p->categorie = null;
            }
        } else {
            $p->categorie = $categorie;
        }
        return $p;
    }

    private static function arrayToMarque(Array $array, Marque $marque = NULL) {
        $p = new Produit();
        $p->idProduit = $array["idProduit"];
        $p->libelle = $array["libelle"];
        $p->description = $array["description"];
        $p->image = $array["image"];
        $p->prix = $array["prix"];
        if ($marque == NULL) {
            if ($array["idMarque"] != NULL) {
                $p->marque = Marque::fetch($array["idMarque"]);
            } else {
                $p->marque = null;
            }
        } else {
            $p->marque = $marque;
        }
        return $p;
    }

    private static $select = "select * from produit";
    private static $selectById = "select * from produit where idProduit = :idProduit";
    private static $insert = "insert into produit (libelle,description,image,prix, idCategorie) values (:libelle,:description,:image,:prix, :idCategorie)";
    private static $update = "update produit set libelle=:libelle,description=:description,image=:image,prix=:prix, idCategorie=:idCategorie where idProduit=:idProduit";
    private static $delete = "delete produit where idProduit=:idProduit";
    private static $selectByIdCategorie = "select * from categorie where idCategorie = :idCategorie";
    private static $selectByIdMarque = "select * from marque where idMarque = :idMarque";

    public static function fetchAll() {
        $collectionProduit = null;
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->query(Produit::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionProduit[] = Produit::arrayToProduit($record);
        }
        return $collectionProduit;
    }

    public static function fetch($idProduit) {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$selectById);
        $pdoStatement->bindParam(":idProduit", $idProduit);
        $pdoStatment->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $produit = Produit::arrayToProduit($record);
        return $produit;
    }

    public function save() {
        if ($this->idProduit == null) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    public function insert() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description", $this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if ($this->categorie != NULL) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        if ($this->marque != NULL) {
            $idMarque = $this->marque->getIdMarque();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->bindParam(":idMarque", $idMarque);
        $pdoStatement->execute();
//        var_dump($pdoStatement->errorInfo());

        $this->idProduit = $pdo->lastInsertId();
    }

    public function update() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$update);
        $pdoStatement->bindParam("idProduit", $this->idProduit);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":description", $this->description);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->bindParam(":prix", $this->prix);
        if ($this->categorie != NULL) {
            $idCategorie = $this->categorie->getIdCategorie();
        }
        if ($this->marque != NULL) {
            $idMarque = $this->marque->getIdMarque();
        }
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->bindParam(":idMarque", $idMarque);
        $pdoStatement->execute();
    }

    public function delete() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Produit::$delete);
        $pdoStatement->bindParam("idProduit", $this->idProduit);
        $resultat = $pdoStatement->execute();
        $nblignesAffectees = $pdoStatement->rowCount();
        if ($nblignesAffectees == 1) {
            $this->getCategorie()->removeProduit($this);
            $this->idProduit = NULL;
        }
        return $resultat;
    }

}
